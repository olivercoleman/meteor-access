// Simple, flexible Meteor package for isomorphic access control. Supports access control and permission checking based on roles, rules, ownership and optionally groups. **Access** can be used for collection- and document-level access control, or any other level you can think of really.

import { Meteor } from 'meteor/meteor';


const AUTHENTICATED = '__auth__';
const UNAUTHENTICATED = '__unauth__';
const OWN = '__own__';

const config = {
  userIdField: 'userId',
  groupIdField: 'groupId',
  fieldAccessField: 'access',
  mongoFieldNameTester: /^[a-zA-Z0-9_.]+$/,
  logDenials: true,
};


const mongoOpTester = /^\$[a-zA-Z]+$/;


const processUserArgument = (user) => {
  // If the user argument wasn't supplied (as opposed to supplied but null).
  if (typeof user == 'undefined') {
    try {
      user = Meteor.user();
    } catch(ex) {
      throw new Error("Unable to determine current user. Perhaps you need to pass in the user(Id) explicitly.");
    }
  }
  else if (typeof user == 'string') {
    user = Meteor.users.findOne(user);
    if (!user) throw new Error("Invalid user id provided.");
  }
  else if (typeof user != 'object' && user !== null) {
    throw new Error("Invalid user argument provided.");
  }
  return user;
}



/**
 * Determine if access is allowed. Can be called in 'rule' mode or
 * 'role' mode. The single params object has properties:
 * @param {Object} rules - The access rules. Mutually exclusive with the role param.
 * @param {string} role - The role to check for. This may be a role defined via
 *   the alanning:roles package or one of Access.AUTHENTICATED or
 *   Access.UNAUTHENTICATED. Mutually exclusive with collection param.
 * @param {string} op - For collection mode. The operation to check for,
 *   typically one of 'view', 'create', 'update', 'delete' but depends on the
 *   access rules specified by the collection.
 * @param {Object} item - For collection mode. The item to check, if applicable.
 *   The item should be an object/document with a user id and/or group id field
 *   (by default `userId` and `groupId`).
 * @param {string} groupId - For role mode. The user group to check in, if applicable.
 * @param {Object|String} user - The user or users id to check for.
 *   If not given the current user is used.
 * @return {boolean} Whether access is allowed.
 */
const allowed = ({rules, role, op, item, groupId, user}) => {
  // If no access control defined for this collection or specific op.
  if (!role && !rules) throw new Error("Neither role nor access rules provided.");
  // If no access control defined for this collection or specific op.
  if (role && rules) throw new Error("Both role and access rules provided, only one allowed.");

  user = processUserArgument(user);

  if (role) {
    if (role == AUTHENTICATED) return user !== null;
    if (role == UNAUTHENTICATED) return user === null;
    return Roles.userIsInRole(user, role, groupId)
  }

  // For each role for which permissions are defined for the collection.
  for (let role in rules) {
    // If the role is allowed to perform the op.
    if (rules[role].includes(op)) {
      if (user) {
        if (role == AUTHENTICATED) {
          return true;
        }
        else if (role == OWN) {
          if (item && (item[config.userIdField] == user._id || item._id == user._id)) return true;
        }
        else {
          // If the item belongs to a group and the the user has the role in that group
          // (or the user has the role in Roles.GLOBAL_GROUP).
          if (Roles.userIsInRole(user, role, item && item[config.groupIdField])) return true;
        }
      }
      else {
        if (role == UNAUTHENTICATED) return true;
      }
    }
  }

  return false;
}



/**
 * Check access for fields in a schema for the specified op. If sub-document
 * fields are specified then the most granular level of access will be determined
 * (if access rules are defined for the sub-document field these will be used, otherwise
 * if the parent (sub-document) field specifies rules these will be used, and so on).
 *
 * Single params object has properties:
 * @param {Object} fields - An array, or other iterable collection, of field names.
 * @param {string} op - The operation to check for, for example 'view', but
 *   depends on the access rules specified by the schema.
 * @param {Object} schema - The schema to check against. Same format as
 *   (aldeed) simpl-schema but with an optional 'access' property. If the schema
 *   provides an 'access' property for a field then this will be used, otherwise
 *   defaultAccessRules will be used for the field.
 * @param {Object} item - The item to check against, if applicable.
 *   The item should be an object/document with a user id and/or group id field
 *   (by default `userId` and `groupId`).
 * @param {Object|String} user - The user or users id to check for.
 * @param {Object} defaultAccessRules - The permission rules to use for fields
 *   that do not define their own. Optional. If not provided then default
 *   behaviour is to deny access.
 * @return {boolean} Whether the specified op may be performed on the given fields.
 */
const fieldsAllowed = ({fields, op, schema, item, user, defaultAccessRules}) => {
  user = processUserArgument(user);
  // Check if access is allowed.
  for (let field of fields) {
    // Should always be allowed to view the _id field (if item access is granted).
    if (op == 'view' && field == '_id') continue;
  
    // Check mongo field name. Overly paranoid security check?
    if (!checkFieldName(field)) return false;

    // Convert mongo array indices to schema array field name.
    field = field.replace(/\.\d+/g, '.$');

    const rules = getAccessRulesForField(schema, field, defaultAccessRules);
    if (!rules || !allowed({rules, op, item, user})) {
      if (config.logDenials) console.warn(`Access.fieldsAllowed: denied on field '${field}' for operator '${op}'.`);
      return false;
    }
  }
  return true;
}



/**
 * Get the fields in a schema for which the specified op may be performed.
 * Only top-level fields are considered.
 * Single params object has properties:
 * @param {string} op - The operation to check for, for example 'view', but
 *   depends on the access rules specified by the schema.
 * @param {Object} schema - The schema to check against. Same format as
 *   (aldeed) simpl-schema but with an optional 'access' property. If the schema
 *   provides an 'access' property for a field then this will be used, otherwise
 *   defaultAccessRules will be used for the field.
 * @param {Object} item - The item to check against, if applicable.
 *   The item should be an object/document with a user id and/or group id field
 *   (by default `userId` and `groupId`).
 * @param {Object|String} user - The user or users id to check for.
 * @return {Array} The fields for which the specified op may be performed.
 */
const getFieldsForOp = ({op, schema, item, user}) => {
  user = processUserArgument(user);

  const fields = [];
  for (const field in schema) {
    if (!field.includes('.') && schema[field].access && allowed({rules: schema[field].access, op, item, user})) {
      fields.push(field);
    }

    // Should always be allowed to view the _id field (if item access is granted).
    if (op == 'view' && !fields.includes('_id')) {
      fields.push('_id');
    }
  }
  return fields;
}



/**
 * Check if a MongoDB modifier may be applied based on the specified field
 * permissions. Single params object has properties:
 * @param {Object} modifier - The MongoDB modifier to check.
 * @param {Object} item - The item to check against, if applicable.
 *   The item should be an object/document with a user id and/or group id field
 *   (by default `userId` and `groupId`).
 * @param {Object} schema - The field schema to check against. Same format as
 *   (aldeed) simpl-schema but with an optional 'access' property. If the schema
 *   provides an 'access' property for a field then this will be used, otherwise
 *   defaultAccessRules will be used for the field.
 * @param {Object|String} user - The user or users id to check for.
 * @param {Object} defaultAccessRules - The permission rules to use for fields
 *   that do not define their own. Optional. If not provided then default
 *   behaviour is to deny access.
 * @return {boolean} Whether the modifier is allowed.
 */
const modifierAllowed = ({modifier, item, schema, user, defaultAccessRules}) => {
  user = processUserArgument(user);

  // Most fields use defaultAccessRules so just determine result for this once for $any operator.
  const defaultAccessRulesResultForAnyOp = !!defaultAccessRules && allowed({rules: defaultAccessRules, op: '$any', item, user});

  // For each mongo update operator.
  for (let op of Object.keys(modifier)) {
    if (!mongoOpTester.test(op)) {
      if (config.logDenials) console.warn(`Access.checkModifier: Expected '${op}' to be a Mongo modifier operator.`);
      return false;
    }
    // Most fields use defaultAccessRules so just determine result for this once for this operator.
    const defaultAccessRulesResult = !!defaultAccessRules && allowed({rules: defaultAccessRules, op, item, user});

    // For each field to be modified by this operator.
    for (let field of Object.keys(modifier[op])) {
      // Check mongo field name. Overly paranoid security check?
      if (!checkFieldName(field)) return false;

      // Convert mongo array indices to schema array field name.
      field = field.replace(/\.\d+/g, '.$');

      const rules = getAccessRulesForField(schema, field, null);

      // If this field (or one of its parents) defines its own access rules.
      if (rules) {
        if (!allowed({rules, op, item, user}) && !allowed({rules, op: '$any', item, user})) {
          if (config.logDenials) console.warn(`Access.checkModifier: denied on field '${field}' for operator '${op}' based on field-specific rules.`);
          return false;
        }
      }
      else if (!defaultAccessRulesResult && !defaultAccessRulesResultForAnyOp) {
        if (config.logDenials) console.warn(`Access.checkModifier: denied on field '${field}' for operator '${op}' based on default rules.`);
        return false;
      }
    }
  }

  return true;
}


// Given a field schema and a (potentially sub-document) field name,
// determine the most granular access rules for the field.
// If no access rules found returns defaultAccessRules.
const getAccessRulesForField = (schema, field, defaultAccessRules) => {
  if (!schema.hasOwnProperty(field)) throw new Error(`Non-existent field specified in access check: '${field}'.`);
  if (schema[field][config.fieldAccessField]) return schema[field][config.fieldAccessField];
  const subDocIndex = field.lastIndexOf('.');
  if (subDocIndex == -1) return defaultAccessRules;
  return getAccessRulesForField(schema, field.slice(0, subDocIndex), defaultAccessRules);
}


const checkFieldName = (field) => {
  if (!config.mongoFieldNameTester.test(field)) {
    if (config.logDenials) console.warn(`Access: expected Mongo field name '${field}' to only contain alphanumeric, underscore and period characters. Modify the Access.config.mongoFieldNameTester regex if you have pathologically-named fields (positive testing).`);
    return false;
  }
  return true;
}



export default { config, allowed, fieldsAllowed, getFieldsForOp, modifierAllowed, AUTHENTICATED, UNAUTHENTICATED, OWN };
