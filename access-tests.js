import { Tinytest } from "meteor/tinytest";
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import Access from 'meteor/ojc:access';

Access.config.logDenials = false;

const Docs = new Meteor.Collection('docs');

if (Meteor.isServer) {
  // Set-up users and roles //////////////////////////////
Meteor.users.remove({});
Meteor.roles.remove({});
const userAdminGroupA = Accounts.createUser({
    email: "a",
    password: "a",
    profile: { name: "a"}
  });

  const userVanilla = Accounts.createUser({
    email: "b",
    password: "b",
    profile: { name: "b"}
  });

  const userAdminGlobal = Accounts.createUser({
    email: "c",
    password: "c",
    profile: { name: "c"}
  });

  Roles.createRole('admin');
  Roles.addUsersToRoles(userAdminGroupA, 'admin', 'groupA');
  Roles.addUsersToRoles(userAdminGlobal, 'admin');

  // Set-up docs ///////////////////////////
  Docs.remove({});
  const doc = Docs.findOne(Docs.insert({}));
  const docuserAdminGroupA = Docs.findOne(Docs.insert({userId: userAdminGroupA}));
  const docuserVanilla = Docs.findOne(Docs.insert({userId: userVanilla}));
  const docGroupA = Docs.findOne(Docs.insert({groupId: 'groupA'}));
  const docGroupB = Docs.findOne(Docs.insert({groupId: 'groupB'}));
  const docuserAdminGroupAGroupA = Docs.findOne(Docs.insert({userId: userAdminGroupA, groupId: 'groupA'}));
  const docuserAdminGroupAGroupB = Docs.findOne(Docs.insert({userId: userAdminGroupA, groupId: 'groupB'}));
  const docuserVanillaGroupA = Docs.findOne(Docs.insert({userId: userVanilla, groupId: 'groupA'}));
  const docuserVanillaGroupB = Docs.findOne(Docs.insert({userId: userVanilla, groupId: 'groupB'}));

  const rules = {
    [Access.UNAUTHENTICATED]: [ ],
    [Access.AUTHENTICATED]: [ 'view', 'create' ],
    [Access.OWN]: [ 'update', 'delete', '$set' ],
    admin: [ 'update', 'delete', '$set', '$unset' ],
  };

  const schema = {
    fieldNoAccess: {
      type: Object,
    },
    'fieldNoAccess.subDocFieldWithRules': {
      type: Number,
      access: rules,
    },
    'fieldNoAccess.subDocFieldWithoutRules': {
      type: Number,
    },
    fieldAdmin: {
      type: Number,
      access: { admin: [ 'view', 'update', 'delete', '$set', '$unset' ] },
    },
    fieldUser: {
      type: Number,
      access: rules,
    },
    fieldUserOnly: {
      type: Number,
      access: { [Access.OWN]: [ 'update', 'delete', '$set' ] },
    },
  }

  const modifierNoAccessField = { $set: { fieldNoAccess: 1 } };
  const modifierOwnField = { $set: { fieldUser: 1 } };
  const modifierOnlyOwnField = { $set: { fieldUserOnly: 1 } };
  const modifierAdminField = { $set: { fieldAdmin: 1, fieldUser: 1  } };


  // Do tests ////////////////

  Tinytest.add("allowed - argument check - no role or rules provided", (test) => {
    test.throws(
      () => Access.allowed({user: null, groupId: 'groupB'}),
      "Neither role nor access rules provided"
    );
  });
  Tinytest.add("allowed - argument check - role and rules provided", (test) => {
    test.throws(
      () => Access.allowed({role: 'admin', rules, user: null, groupId: 'groupB'}),
      "Both role and access rules provided"
    );
  });

  Tinytest.add("allowed - role - group - no user", (test) => {
    test.isFalse(Access.allowed({role: 'admin', user: null, groupId: 'groupB'}));
  });
  Tinytest.add("allowed - role - group - wrong user", (test) => {
    test.isFalse(Access.allowed({role: 'admin', user: userVanilla, groupId: 'groupB'}));
  });
  Tinytest.add("allowed - role - group - wrong group", (test) => {
    test.isFalse(Access.allowed({role: 'admin', user: userAdminGroupA, groupId: 'groupB'}));
  });
  Tinytest.add("allowed - role - group - right", (test) => {
    test.isTrue(Access.allowed({role: 'admin', user: userAdminGroupA, groupId: 'groupA'}));
  });
  Tinytest.add("allowed - role - group - right global", (test) => {
    test.isTrue(Access.allowed({role: 'admin', user: userAdminGlobal}));
  });

  Tinytest.add("allowed - role - no group - no user", (test) => {
    test.isFalse(Access.allowed({role: 'admin', user: null}));
  });
  Tinytest.add("allowed - role - no group - wrong user", (test) => {
    test.isFalse(Access.allowed({role: 'admin', user: userAdminGroupA}));
  });
  Tinytest.add("allowed - role - no group - wrong role", (test) => {
    test.isFalse(Access.allowed({role: 'blah', user: userAdminGlobal}));
  });
  Tinytest.add("allowed - role - no group - right", (test) => {
    test.isTrue(Access.allowed({role: 'admin', user: userAdminGlobal}));
  });


  Tinytest.add("allowed - rule - auth - no user", (test) => {
    test.isFalse(Access.allowed({rules, op: 'view', user: null}));
  });
  Tinytest.add("allowed - rule - auth - wrong op", (test) => {
    test.isFalse(Access.allowed({rules, op: 'blah', user: userAdminGroupA}));
  });
  Tinytest.add("allowed - rule - auth - right", (test) => {
    test.isTrue(Access.allowed({rules, op: 'view', user: userAdminGroupA}));
  });

  Tinytest.add("allowed - rule - own - no doc", (test) => {
    test.isFalse(Access.allowed({rules, op: 'update', user: userAdminGroupA}));
  });
  Tinytest.add("allowed - rule - own - no user for doc", (test) => {
    test.isFalse(Access.allowed({rules, op: 'update', item: doc, user: userAdminGroupA}));
  });
  Tinytest.add("allowed - rule - own - wrong user for doc", (test) => {
    test.isFalse(Access.allowed({rules, op: 'update', item: docuserVanilla, user: userAdminGroupA}));
  });
  Tinytest.add("allowed - rule - own - wrong op", (test) => {
    test.isFalse(Access.allowed({rules, op: 'blah', item: docuserVanilla, user: userVanilla}));
  });
  Tinytest.add("allowed - rule - own - allowed", (test) => {
    test.isTrue(Access.allowed({rules, op: 'update', item: docuserVanilla, user: userVanilla}));
  });

  Tinytest.add("allowed - rule - group - no group for doc", (test) => {
    test.isFalse(Access.allowed({rules, op: 'update', item: doc, user: userAdminGroupA}));
  });
  Tinytest.add("allowed - rule - group - wrong user for doc", (test) => {
    test.isFalse(Access.allowed({rules, op: 'update', item: docGroupA, user: userVanilla}));
  });
  Tinytest.add("allowed - rule - group - wrong op", (test) => {
    test.isFalse(Access.allowed({rules, op: 'blah', item: docGroupA, user: userAdminGroupA}));
  });
  Tinytest.add("allowed - rule - group - allowed", (test) => {
    test.isTrue(Access.allowed({rules, op: 'update', item: docGroupA, user: userAdminGroupA}));
  });

  Tinytest.add("allowed - rule - global group - wrong op", (test) => {
    test.isFalse(Access.allowed({rules, op: 'blah', item: doc, user: userAdminGlobal}));
  });
  Tinytest.add("allowed - rule - global group - right", (test) => {
    test.isTrue(Access.allowed({rules, op: 'delete', item: doc, user: userAdminGlobal}));
  });




  ///// fieldsAllowed({fields, op, schema, item, user})]

  Tinytest.add("fieldsAllowed - invalid field", (test) => {
    test.throws(
      () => Access.fieldsAllowed({ fields: [ 'blah' ], op: 'view', schema, user: userAdminGlobal }),
      "Non-existent field"
    );
  });
  Tinytest.add("fieldsAllowed - no access property", (test) => {
    test.isFalse(Access.fieldsAllowed({ fields: [ 'fieldNoAccess' ], op: 'view', schema, user: userAdminGlobal }));
  });

  Tinytest.add("fieldsAllowed - disallowed - top field", (test) => {
    test.isFalse(Access.fieldsAllowed({ fields: [ 'fieldAdmin' ], op: 'view', schema, user: userVanilla }));
  });
  Tinytest.add("fieldsAllowed - disallowed - sub field with rules", (test) => {
    test.isFalse(Access.fieldsAllowed({ fields: [ 'fieldNoAccess.subDocFieldWithRules' ], op: 'view', schema, user: null }));
  });
  Tinytest.add("fieldsAllowed - disallowed - sub field without rules", (test) => {
    test.isFalse(Access.fieldsAllowed({ fields: [ 'fieldNoAccess.subDocFieldWithoutRules' ], op: 'view', schema, user: userAdminGlobal }));
  });
  Tinytest.add("fieldsAllowed - allowed - top field", (test) => {
    test.isTrue(Access.fieldsAllowed({ fields: [ 'fieldUser' ], op: 'view', schema, user: userVanilla }));
  });
  Tinytest.add("fieldsAllowed - allowed - sub field with rules", (test) => {
    test.isTrue(Access.fieldsAllowed({ fields: [ 'fieldNoAccess.subDocFieldWithRules' ], op: 'view', schema, user: userVanilla }));
  });




  ///// getFieldsForOp({op, schema, item, user})
  Tinytest.add("getFieldsForOp - unauthenticated (no fields)", (test) => {
    test.equal(
      Access.getFieldsForOp({ op: 'view', schema, user: null }),
      []
    );
  });
  Tinytest.add("getFieldsForOp - authenticated (one field)", (test) => {
    test.equal(
      Access.getFieldsForOp({ op: 'view', schema, user: userVanilla }),
      ['fieldUser']
    );
  });
  Tinytest.add("getFieldsForOp - admin (multiple fields)", (test) => {
    test.equal(
      Access.getFieldsForOp({ op: 'view', schema, user: userAdminGlobal }),
      ['fieldAdmin', 'fieldUser']
    );
  });




  ///// modifierAllowed({item, modifier, user, schema, defaultAccessRules})

  // Checking default rules behaviour.
  Tinytest.add("modifierAllowed - no access property - no default rules", (test) => {
    test.isFalse(Access.modifierAllowed({ schema, modifier: modifierNoAccessField, user: userAdminGroupA }));
  });
  Tinytest.add("modifierAllowed - no access property - default rules - no item - wrong user", (test) => {
    test.isFalse(Access.modifierAllowed({ schema, modifier: modifierNoAccessField, user: userVanilla, defaultAccessRules: rules }));
  });
  Tinytest.add("modifierAllowed - no access property - default rules - no item - right user", (test) => {
    test.isTrue(Access.modifierAllowed({ schema, modifier: modifierNoAccessField, user: userAdminGlobal, defaultAccessRules: rules }));
  });
  Tinytest.add("modifierAllowed - no access property - default rules - item - wrong user", (test) => {
    test.isFalse(Access.modifierAllowed({ schema, modifier: modifierNoAccessField, user: userAdminGroupA, item: docuserVanillaGroupB, defaultAccessRules: rules }));
  });
  Tinytest.add("modifierAllowed - no access property - default rules - item - right user", (test) => {
    test.isTrue(Access.modifierAllowed({ schema, modifier: modifierNoAccessField, user: userVanilla, item: docuserVanillaGroupB, defaultAccessRules: rules }));
  });
  // Checking to make sure default rules don't override field-specific rules.
  Tinytest.add("modifierAllowed - user only field - default rules - no item", (test) => {
    test.isFalse(Access.modifierAllowed({ schema, modifier: modifierOnlyOwnField, user: userAdminGlobal, defaultAccessRules: rules }));
  });
  Tinytest.add("modifierAllowed - user only field - default rules - wrong user", (test) => {
    test.isFalse(Access.modifierAllowed({ schema, modifier: modifierOnlyOwnField, user: userAdminGlobal, item: docuserVanillaGroupB, defaultAccessRules: rules }));
  });
  Tinytest.add("modifierAllowed - user only field - default rules - right user", (test) => {
    test.isTrue(Access.modifierAllowed({ schema, modifier: modifierOnlyOwnField, user: userVanilla, item: docuserVanillaGroupB, defaultAccessRules: rules }));
  });

  Tinytest.add("modifierAllowed - user field - no item - wrong user", (test) => {
    test.isFalse(Access.modifierAllowed({ schema, modifier: modifierOwnField, user: userAdminGroupA }));
  });
  Tinytest.add("modifierAllowed - user field - no item - own user", (test) => {
    test.isFalse(Access.modifierAllowed({ schema, modifier: modifierOwnField, user: userVanilla }));
  });
  Tinytest.add("modifierAllowed - user field - no item - global admin user", (test) => {
    test.isTrue(Access.modifierAllowed({ schema, modifier: modifierOwnField, user: userAdminGlobal }));
  });

  Tinytest.add("modifierAllowed - user field - item - wrong user", (test) => {
    test.isFalse(Access.modifierAllowed({ schema, modifier: modifierOwnField, user: userAdminGroupA, item: docuserVanillaGroupB }));
  });
  Tinytest.add("modifierAllowed - user field - item - own user", (test) => {
    test.isTrue(Access.modifierAllowed({ schema, modifier: modifierOwnField, user: userVanilla, item: docuserVanillaGroupB }));
  });
  Tinytest.add("modifierAllowed - user field - item - global admin user", (test) => {
    test.isTrue(Access.modifierAllowed({ schema, modifier: modifierOwnField, user: userAdminGlobal, item: docuserVanillaGroupB }));
  });

  Tinytest.add("modifierAllowed - admin+user field - no item - wrong user group", (test) => {
    test.isFalse(Access.modifierAllowed({ schema, modifier: modifierAdminField, user: userAdminGroupA }));
  });
  Tinytest.add("modifierAllowed - admin+user field - no item - wrong user vanilla", (test) => {
    test.isFalse(Access.modifierAllowed({ schema, modifier: modifierAdminField, user: userVanilla }));
  });
  Tinytest.add("modifierAllowed - admin+user field - no item - global admin user", (test) => {
    test.isTrue(Access.modifierAllowed({ schema, modifier: modifierAdminField, user: userAdminGlobal }));
  });

  Tinytest.add("modifierAllowed - admin+user field - item - wrong user", (test) => {
    test.isFalse(Access.modifierAllowed({ schema, modifier: modifierAdminField, user: userAdminGroupA, item: docuserVanillaGroupB }));
  });
  Tinytest.add("modifierAllowed - admin+user field - item - own user", (test) => {
    test.isFalse(Access.modifierAllowed({ schema, modifier: modifierAdminField, user: userVanilla, item: docuserVanillaGroupB }));
  });
  Tinytest.add("modifierAllowed - admin+user field - item - global admin user", (test) => {
    test.isTrue(Access.modifierAllowed({ schema, modifier: modifierAdminField, user: userAdminGlobal, item: docuserVanillaGroupB }));
  });
}
