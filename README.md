
# Access

A simple, flexible Meteor package for isomorphic access control. Supports access control and permission checking based on roles, rules, ownership and optionally groups. **Access** can be used for collection, document and field-level access control. Or any other item you can think to attach permissions to.

## Getting started

### Installation

Add one of the built-in accounts packages so the Meteor.users collection exists. For example:
```sh
meteor add accounts-password
```

Add the alanning:roles permissions package.
```sh
meteor add alanning:roles
```

Add this package to your project.
```sh
meteor add ojc:access
```

### Roles and groups

It is a good idea to understand the [alanning:roles package](https://atmospherejs.com/alanning/roles) before proceeding to the examples or API.


## Examples

All examples below assume that you have imported the module, for example:
```js
import Access from 'meteor/ojc:access'
```

### General access checking

The function `Access.allowed` may be used to perform role, document, and collection-level access checking, or other general access checking for anything you could attach access rules to.

#### Access check modes

`Access.allowed` can be used in two modes: *role* access checking; and *rule* access checking. Both modes can be group-aware. If no group is specified for an access check then the default `Roles.GLOBAL_GROUP` group (created by the alanning:roles package) is assumed.

#### Role access mode

Role access checks simply allow checking if a user (the current user by default) has a specific role (roles being defined via the alanning:Roles package). This is very similar to `Roles.userIsInRole` from the alanning:Roles package but also allows for convenient checking of authenticated and unauthenticated status:

```js
// Check for unauthenticated status for the current user.
Access.allowed({role: Access.UNAUTHENTICATED});

// Check for authenticated status for the current user.
Access.allowed({role: Access.AUTHENTICATED});

// Check if the current user has the 'myRole' role.
Access.allowed({role: 'myRole'});

// Check if a specified user has the 'myRole' role.
Access.allowed({role: 'myRole', user: userToCheck});

// Check if a specified user has the 'myRole' role in the 'myGroup' group.
// (This is the bit where you actually read the docs for alanning:roles.
Access.allowed({role: 'myRole', user: userToCheck, groupId: groupToCheckIn});
```

#### Rule access mode

You can also check access based on a set of rules. Rules are described with an object keyed by role names and/or the special 'roles' provided by Access, namely `UNAUTHENTICATED`, `AUTHENTICATED` and `OWN`. For example:
```js
{
  // Says that users not logged in should only be allowed to view the items.
  [Access.UNAUTHENTICATED]: [ 'view' ],
  // Says that logged-in users should only be allowed to create the items.
  [Access.AUTHENTICATED]: [ 'create' ],
  // Says that a user who owns an item should be able to update the item.
  // Ownership of an item is determined by a field containing a user id (`userId` by default).
  [Access.OWN]: [ 'update' ],
  // Says that an admin should be able to update and delete the items - taking groups into account if applicable.
  admin: [ 'update', 'delete'],
}
```

You can define any operations and roles with any names you please. For example:
```js
const myCustomRules = {
  ...
  [Access.OWN]: [ 'view', 'delete', 'banana', 'myConvoluted_and_Inconsistently Named Rule' ],
  editorsWithLongBeards: [ 'updateAny', 'deleteAny', 'pullLongBeers' ],
  ...
}
```

##### Collection-level access control
You can define your rules anywhere you like and use them any way you like. However a typical usage pattern is to associate rules with a collection for collection-level access control. For example:
```js
Docs = new Meteor.Collection('docs');
Docs.access = {
  ...Access.defaultRules,
  myCustomRole: ['view', 'delete', 'eat', 'repeat'],
}
```

Then in a publish function you could do:
```js
Meteor.publish('allTheDocs', function() {
  if (!Access.allowed({rules: Docs.access, op: 'view'})) {
    console.warn("Unauthorised access attempt.");
    return;
  }
  return Docs.find();
});
```

And on the client you might do something like:
```js
const showEatButton = Access.allowed({rules: Docs.access, op: 'eat'});
...
```

Or if documents are owned by users (and your rules define access for the `Access.OWN` 'role') then the check must include the `item` argument and the document must have a field storing a user id (called `userId` by default):
```js
const doc = Docs.findOne({...});
const showRepeatButton = Access.allowed({rules: Docs.access, op: 'repeat', item: doc});
...
```

##### Document-level access control

If document-level access control is required then each document could include its own access rules object. For example if the access permissions are stored in the field `access`:
```js
const doc = Docs.findOne({...});
const showDeleteButton = Access.allowed({rules: doc.access, op: 'delete', item: doc});
...
```

##### Groups

If you're not using groups then you can safely ignore this section.

Rule-based access checks may also take groups into consideration. The items (typically documents) to be checked must have a field containing a group id. By default this field is called `groupId`. For example the below check will be satisfied if the user is in the `myGroup` group but not otherwise.
```js
// Add the current user to the admin role in the myGroup group.
Roles.addUsersToRoles(Meteor.userId(), 'admin', 'myGroup');
...
const doc = Docs.findOne({...});
console.log(doc.groupId); // => "myGroup"
if (Access.allowed({rules: Access.defaultRules, op: 'delete', item: doc})) {
  ...
}
```

##### Super administrators
If the rules allow an operation for some role and the user has that role in the `Roles.GLOBAL_GROUP` group then the access check will effectively ignore the group specified in the item. This is useful for creating 'super-admin' roles and users.


##### Meteor.users Collection
Ownership of a document is denoted by a field of the document storing a user id (`userId` by default), however the `_id` field of a document is also checked, which allows for access control for the `Meteor.users` collection (the user id of a document in `Meteor.users` is the document `_id`).


### Checking MongoDB modifiers

The function `Access.modifierAllowed` may be used to determine whether a user may apply a MongoDB modifier based on a field schema specifying access rules, and optionally with respect to a specific document. Each field in the schema may specify a property defining role-based access rules, with each role listing the allowed MongoDB operations (but may include custom operations for other purposes). The default name for this property is `access` but this can be configured (see end of API section below).
For example:
```js
const schema = {
  myField: {
    access: {
      [Access.OWN]: [ '$set', '$unset', '$push' ],
      ...
    },
  },
  ...
}
```
If a field in the schema does not specify access rules then default access rules may be provided as an argument (see the API below). If a field in the schema does not specify access rules and no default access rules are provided then access will be denied. An error will occur if the modifier specifies a field not listed in the schema or is otherwise invalid.

For example to check a client-supplied modifier argument to a Meteor method:
```js
Meteor.methods({
  update: function update(id, modifier) {
    check(id, String);
    check(modifier, Object);
    const doc = MyCollection.findOne(id);
    if (!Access.modifierAllowed({modifier, item: doc, user: this.userId, schema})) throw new Error("Not allowed.");
    MyCollection.update(id, modifier);
  },
});
```

The field schema is compatible with the popular [SimpleSchema package](https://www.npmjs.com/package/simpl-schema). If you use SimpleSchema you can do something like the below to allow an `access` property to be added as a schema option.
```js
import Access from './access';
SimpleSchema.extendOptions(['access']);
```


### Checking access for a set of fields

The function `Access.fieldsAllowed` checks access for fields in a schema for the specified operation. If sub-document fields are specified then the most granular level of access will be determined: if access rules are defined for the sub-document field these will be used, otherwise if the parent (sub-document) field specifies rules these will be used, and so on.

Note: if  `op` == `view` then the `_id` field is automatically allowed.

For example in a generic publication function that allows specifying the fields to return you could check the list of fields (assuming you've defined a 'view' operation for fields in a schema):
```js
// Publish specified fields for documents.
Meteor.publish('myPub', function myPub(fields) {
  check(fields, [String]);
  if (!Access.fieldsAllowed({fields, op: 'view', schema, user: this.userId})) throw "Not allowed."
  // Convert array of field names to MongoDB field projection.
  fields = fields.reduce((fieldsObject, field) => { return {...fieldsObject, [field]: 1 } }, {});
  return MyDocs.find({}, { fields });
});
```


### Getting a set of fields on which an operation may be performed.

The function `Access.getFieldsForOp` returns the fields in a schema for which the specified operation may be performed. Only top-level fields are considered.

Note: if  `op` == `view` then the `_id` field is automatically included.

For example in a publication function you could do something like below to only return fields visible to the current user (assuming you've defined a 'view' operation for fields in a schema):
 ```js
 Meteor.publish('myPub', function myPub() {
   let fields = Access.getFieldsForOp({ op: 'view', schema, user: this.userId });

   // Convert array of field names to MongoDB field projection.
   fields = fields.reduce((fieldsObject, field) => { return {...fieldsObject, [field]: 1 } }, {});
   return MyDocs.find({}, { fields });
 });
 ```


## Really important note

In the examples above **Access** is used on the client to determine whether a user should be allowed to perform some operation and hence whether, for example, to show a delete button. **You must still perform the same access checks server-side!** The client may easily be modified, or other methods can be used to send requests to the server that should not be allowed.


## API


### `Access.allowed(params)`
Check if access is allowed. `allowed` may be called in two modes: *role* and *rule* mode. `params` is an object with different properties based on the mode:

#### Role mode `params`
- `role` (String) - The role to check for. This may be a role defined via the alanning:roles package or one of `Access.AUTHENTICATED` or `Access.UNAUTHENTICATED`.
- `groupId` (String) - The user group to check in, if applicable. Optional.

#### Rule mode `params`
- `rules` (Object) - The access rules.
- `op` (String) - The operation to check for, typically one of 'view', 'create', 'update', 'delete' but depends on the access rules specified.
- `item` (Object) - The item to check, if applicable. Required for `Access.OWN` rules and for group-based checks. The item should have a user and/or group id field (by default `userId` and `groupId` respectively). Optional.
- `user` (Object|String) - The user or user id to check for. If not given the current user is used.

Returns `true` iff access is allowed, `false` otherwise.


### `Access.fieldsAllowed(params)`
Check access for fields in a schema for the specified operation. If sub-document fields are specified then the most granular level of access will be determined (if access rules are defined for the sub-document field these will be used, otherwise if the parent (sub-document) field specifies rules these will be used, and so on).

`params` is an object with the following properties:
- `fields` (Object) - An array, or other iterable collection, of field names.
- `op` (String) The operation to check for, for example 'view', but
  depends on the access rules specified by the schema.
- `schema` (Object) - The schema to check against. Same format as
  (aldeed) simpl-schema but with an optional 'access' property. If the schema
  provides an 'access' property for a field then this will be used, otherwise
  defaultAccessRules will be used for the field.
- `item` (Object) - The item to check against, if applicable.
  The item should be an object/document with a user id and/or group id field
  (by default `userId` and `groupId`).
- `user` (Object|String) - The user or users id to check for.
- `defaultAccessRules` (Object) - The permission rules to use for fields
  that do not define their own. Optional. If not provided then default
  behaviour is to deny access.

Returns `true` iff the specified operation may be performed on the given fields, otherwise `false`.


### `Access.getFieldsForOp(params)`
Get the fields in a schema for which the specified operation may be performed. Only top-level fields are considered.

`params` is an object with the following properties:
- `op` (String) The operation to check for, for example 'view', but
  depends on the access rules specified by the schema.
- `schema` (Object) - The schema to check against. Same format as
  (aldeed) simpl-schema but with an optional 'access' property. If the schema
  provides an 'access' property for a field then these rules will be used, otherwise
  the field will be excluded.
- `item` (Object) - The item to check against, if applicable.
  The item should be an object/document with a user id and/or group id field
  (by default `userId` and `groupId`).
- `user` (Object|String) - The user or users id to check for.

Returns an array of the (top-level) field names for which the specified operation may be performed.


### `Access.modifierAllowed(params)`
Check if a MongoDB modifier may be applied based on the specified field permissions.

`params` is an object with the following properties:
- `modifier` (Object) - The MongoDB modifier to check.
- `item` (Object) - The document or item to check against, if applicable. The item should be an object/document with a user id and/or group id field (by default `userId` and `groupId`).
- `schema` (Object) - The field schema to check against. Same format as (aldeed) simpl-schema but with an optional `access` property. If the schema provides an `access` property for field then this will be used, otherwise `defaultAccessRules` will be used for the field.
- `user` (Object|String) user - The user or users id to check for, or null for no user.
- `defaultAccessRules` (Object) - The permission rules to use for fields that do not define their own. Optional. If not provided then default behaviour is to deny access.

Returns `true` iff the modifier is allowed, `false` otherwise.


### Setting the user, group id, and field schema access field/property names

**Access** uses certain fields/properties on passed documents, items, and schemas. The names of these fields/properties have defaults, but these can be configured, for example:
```js
Access.config.userIdField = 'myUserIdField';
```
`Access.config` has the following properties:
- `userIdField`: the user id field name (for items passed in rule mode to `Access.allowed`). Default is 'userId'.
- `groupIdField`: the group id field name (for items passed in rule mode to `Access.allowed`). Default is 'groupId'.
- `fieldAccessField`: property name for the access rules for field-level access (used by modifierAllowed, fieldsAllowed and getFieldsForOp functions). Default is 'access'.



## Development

Please post issues and pull requests at https://gitlab.com/olivercoleman/meteor-access

To run the tests do from within the source folder:
```js
meteor test-packages ./
```
