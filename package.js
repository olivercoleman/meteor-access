Package.describe({
  name: 'ojc:access',
  version: '1.3.0',
  // Brief, one-line summary of the package.
  summary: 'Isomorphic role and rule-based access control.',
  // URL to the Git repository containing the source code for this package.
  git: 'https://gitlab.com/olivercoleman/meteor-access',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.6.1');
  api.use('ecmascript');
  api.use('mongo');
  api.use('alanning:roles@1.2.16');
  //api.export('Access');
  api.mainModule('access.js');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('alanning:roles');
  api.use('accounts-password');
  api.use('ojc:access');
  api.mainModule('access-tests.js');
});
